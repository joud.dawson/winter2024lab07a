import java.util.Random;

public class Deck {
    private Card[] cards;
    private int numberOfCards;
    private Random rng;

    public Deck() {
        rng = new Random();
        numberOfCards = 52;
        cards = new Card[numberOfCards];

        String[] suits = { "Hearts", "Diamonds", "Clubs", "Spades" };
        String[] values = { "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack",
                "Queen", "King" };

        for (int i = 0; i < suits.length; i++) {
            for (int j = 0; j < values.length; j++) {
                cards[i * values.length + j] = new Card(suits[i], values[j]);
            }
        }
    }

    // it should return number of cards since this.card.length is a constant 52 but
    // numberofcards can decrease
    public int length() {
        return numberOfCards;
    }

    public Card drawTopCard() {
        if (numberOfCards > 0) {
            Card topCard = cards[numberOfCards - 1]; // last pos of numberOfCards
            numberOfCards--; // reduce by 1
            return topCard;
        } else {
            System.out.println("Deck is empty.");
            return null; // returns nothign if deck is empty (i gotta return something)
        }
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < numberOfCards; i++) {
            result += cards[i].toString() + "\n";
        }
        return result;
    }

    public void shuffle() {
        for (int i = 0; i < numberOfCards; i++) {
            int randomIndex = i + rng.nextInt(numberOfCards - i);
            Card temp = cards[i];
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }
}