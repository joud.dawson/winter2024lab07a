public class Card {
    private String suit;
    private String value;

    public Card(String suit, String value) {
        this.suit = suit;
        this.value = value;
    }

    // getters
    public String getSuit() {
        return suit;
    }

    public String getValue() {
        return value;
    }

    // tostring method

    public String toString() {
        return value + " of " + suit;
    }

    public double calculateScore() {
        double rankScore = 0.0;
        switch (value) {
            case "Ace":
                rankScore = 1.0;
                break;
            case "Two":
                rankScore = 2.0;
                break;
            case "Three":
                rankScore = 3.0;
                break;
            case "Four":
                rankScore = 4.0;
                break;
            case "Five":
                rankScore = 5.0;
                break;
            case "Six":
                rankScore = 6.0;
                break;
            case "Seven":
                rankScore = 7.0;
                break;
            case "Eight":
                rankScore = 8.0;
                break;
            case "Nine":
                rankScore = 9.0;
                break;
            case "Ten":
                rankScore = 10.0;
                break;
            case "Jack":
                rankScore = 11.0;
                break;
            case "Queen":
                rankScore = 12.0;
                break;
            case "King":
                rankScore = 13.0;
                break;
            default:
                return -1.0; // Invalid value
        }

        double suitScore = 0.0;
        switch (suit) {
            case "Hearts":
                suitScore = 0.4;
                break;
            case "Spades":
                suitScore = 0.3;
                break;
            case "Diamonds":
                suitScore = 0.2;
                break;
            case "Clubs":
                suitScore = 0.1;
                break;
            default:
                return -1.0; // Invalid suit
        }

        return rankScore + suitScore;
    }
}