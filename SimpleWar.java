public class SimpleWar {

    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.shuffle();

        int playerOnePoints = 0;
        int playerTwoPoints = 0;

        int round = 0;
        while (deck.length()>0) {

            round++;

            System.out.println(" ---------Round " + (round) + "---------");
            // print points
            System.out.println(
                    "\nPlayer 1 points: " + playerOnePoints + " || " + "Player 2 points: " + playerTwoPoints + "\n");

            Card card1 = deck.drawTopCard();
            deck.shuffle();
            Card card2 = deck.drawTopCard();
            System.out.println("Player 1 Card: " + (card1) + ", worth " + card1.calculateScore() + " points.");
            System.out.println("Player 2 Card: " + (card2) + ", worth " + card2.calculateScore() + " points.");

            if (card1.calculateScore() > card2.calculateScore()) {
                System.out.println("★ Player 1 with " + (card1) + " wins.");
                playerOnePoints++;

            } else {
                System.out.println("★ Player 2 with " + (card2) + " wins.");
                playerTwoPoints++;
            }
            // print points
            System.out.println(
                    "\nPlayer 1 points: " + playerOnePoints + " || " + "Player 2 points: " + playerTwoPoints + "\n");

        }

        if (playerOnePoints>playerTwoPoints) {
            System.out.println("Congratulations Player 1 on your victory!");
        } else {
            System.out.println("Congratulations Player 2 on your victory!");
        }
    }

}